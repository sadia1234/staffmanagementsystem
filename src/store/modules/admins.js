

const state = {
    admins: [
        {
            id: 100,
            firstname: 'Matthew',
            lastname:'Roberts',
            email: "a@gmail.com",
            phone: "8827638756123",
            birthdate:null,
            joiningdate:null,
            address:'Badda',
            city:'Dhaka',
            status: "admin",
            img:require('@/assets/blank-profile-picture-973460_640.png')
        },
        

        
        

    ]
};

const getters = {
    allAdmins: (state) => state.admins,
   
};

const actions = {
};

const mutations = {

    addAdmin(state, admin) {
        state.admins.push(admin)
        return true;
    },
    getadminById: (state) => (id) => {
        return state.admins.find(admin => admin.id === id)
      },
    updateAdmin: (state, payload) => {
        const { id, firstname,lastname, email, phone,img,city,address,birthdate,joiningdate } = payload
        const admin = state.admins.find(p => p.id === id)
        admin.firstname = firstname;
        admin.lastname = lastname;
        admin.email = email;
        admin.phone = phone;
        admin.img=img;
        admin.city=city;
        admin.address=address;
        admin.birthdate=birthdate;
        admin.joiningdate=joiningdate;
    },
    DELETE_Admin(state, id) {
        const index = state.admins.findIndex(admin => admin.id == id)
        state.admins.splice(index, 1)
    }
};

export default {
    state,
    getters,
    actions,
    mutations,
}