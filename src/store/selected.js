const state = {
    selectedPerson: 
        {
           
        },
            
};

const getters = {
    selectedPerson: (state) => state.selectedPerson,
};

const actions = {
};

const mutations = {
        Set_Person_Selected: (state, data) => {
          // state.userData.assign(data)
          state.selectedPerson = Object.assign({}, data)
        }
    
};

export default {
    state,
    getters,
    actions,
    mutations,
}