import Vuex from 'vuex';
import Vue from 'vue';
import admins from './modules/admins';
import staffs from './modules/staffs';
import selectedPerson from './selected';
// import createPersistedState from 'vuex-persistedstate'
Vue.use(Vuex);

export default new Vuex.Store({
 modules:{
     admins,
     staffs,
     selectedPerson
 },
//  plugins: [createPersistedState({
//     storage: window.sessionStorage,
// })],
});