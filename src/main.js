import Vue from 'vue';
import App from './App.vue';
import store from './store';
import vuetify from './plugins/vuetify';
import UUID from 'vue-uuid' 
import VueRouter from 'vue-router';
import Routes from './routes.js';
import moment from 'moment'

Vue.prototype.moment = moment


Vue.use(UUID); 
Vue.use(VueRouter);

Vue.config.productionTip = false

const router = new VueRouter({
  mode: 'history',
  routes: Routes
});

new Vue({
  store,
  vuetify,
  router:router,
  render: h => h(App)
}).$mount('#app')
